import numpy as np
import math

# prints formatted price
def formatPrice(n):
    return ("-$" if n < 0 else "$") + "{0:.2f}".format(abs(n))

# returns the vector containing stock data from a fixed file
def getStockDataVec(key):
    with open("data/" + key + ".csv", "r") as F:
        lines=F.readlines()
        return [float(line.strip().split(",")[4]) for line in lines[1:]]
        """
    vec = []
    lines = open("data/" + key + ".csv", "r").read().splitlines()

    for line in lines[1:]:
        vec.append(float(line.split(",")[4]))

    return vec
        """

# returns the sigmoid
def sigmoid(x):
    try:
        if x < 0:
            return 1 - 1 / (1 + math.exp(x))
        return 1 / (1 + math.exp(-x))
    except OverflowError as err:
        print("Overflow err: {0} - Val of x: {1}".format(err, x))
    except ZeroDivisionError:
        print("division by zero!")
    except Exception as err:
        print("Error in sigmoid: " + err)
    

data_sigmoid=[]
# returns an an n-day state representation ending at time t
def getState(data, t, n):
    """
    global data_sigmoid
    if data_sigmoid==[]:
        for i in range(len(data)-1):
            data_sigmoid.append(sigmoid(data[i + 1] - data[i]))
        data_sigmoid=np.array(data_sigmoid)
    d = t-n+1
    block = data_sigmoid[d:t] if d >= 0 else np.append(np.array(-d * [0.5]) , data_sigmoid[0:t]) #sigmoid(x-x)=0.5
    return np.array([block])
    
    
    """
    d = t - n + 1
    block = data[d:t + 1] if d >= 0 else -d * [data[0]] + data[0:t + 1] # pad with t0
    res = []
    for i in range(n - 1):
        res.append(sigmoid(block[i + 1] - block[i]))

    return np.array([res])
    
