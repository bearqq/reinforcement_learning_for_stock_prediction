import keras
from keras.models import load_model

from agent.agent import Agent
from functions import *
import sys

try:
    if len(sys.argv) != 3:
        print ("Usage: python evaluate.py [stock] [model]")
        exit()

    stock_name, model_name = sys.argv[1], sys.argv[2]
    model = load_model("models/" + model_name)
    window_size = model.layers[0].input.shape.as_list()[1]

    agent = Agent(window_size, True, model_name)
    data = getStockDataVec(stock_name)
    l = len(data) - 1
    batch_size = 128

    state = getState(data, 0, window_size + 1)
    total_profit = 0
    agent.inventory = []

    for t in range(l):
        action = agent.act(state)

        # sit
        next_state = getState(data, t + 1, window_size + 1)
        reward = 0

        if action == 1: # buy
            if len(agent.inventory)<10:
                agent.inventory.append(data[t])
                print ("Buy: " + formatPrice(data[t])+' @'+str(t))
            else:
                print("Buy*: " + formatPrice(data[t])+' @'+str(t))

        elif action == 2:
            if len(agent.inventory) > 0: # sell
                bought_price = agent.inventory.pop(0)
                reward = max(data[t]*0.995 - bought_price, 0)
                total_profit += data[t]*0.995 - bought_price
                print ("Sell: " + formatPrice(data[t]) + " | Profit: " + formatPrice(data[t]*0.995 - bought_price)+' @'+str(t))
            else:
                print("Sell*: " + formatPrice(data[t])+' @'+str(t))

        done = True if t == l - 1 else False
        agent.memory.append((state, action, reward, next_state, done))
        state = next_state

        if done:
            print ("--------------------------------")
            print (stock_name + " Total Profit: " + formatPrice(total_profit))
            print ("--------------------------------")
        
        if len(agent.memory) > batch_size:
                agent.expReplay(batch_size) 
except Exception as e:
    print(str(e))
finally:
    pass
    #exit()
